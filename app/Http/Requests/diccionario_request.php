<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class diccionario_request extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'archivo'=> 'required',
        ];
    }

    public function messages()
    {
        return [
            'archivo.required' => 'Es necesario cargar un archivo de diccionario',
        ];
    }
}
