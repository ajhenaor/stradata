<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App;
use App\Diccionario;
use App\Http\Requests\diccionario_request;

class diccionario_controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Auth::check()) {
            return \View::make('diccionario');
        }else{
            return \Redirect::to('/auth/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(diccionario_request $request)
    {
        $archivo = $request->file('archivo');
        $excel = App::make('excel');
        $excel->setDelimiter(';');
        $excel->load($archivo, function($reader) {
            foreach ($reader->get() as $persona) {
                echo $persona."<br>";
                $registro = Diccionario::where('identificacion', '=', $persona->id)
                                        ->where('nombre', '=', $persona->nombre)->first();
                if(!is_null($registro)){
                    $registro->updated_at = $registro->freshTimestamp();
                    $registro->save();
                }else{
                    Diccionario::create([
                        'identificacion' => $persona->id,
                        'nombre' => $persona->nombre
                    ]);
                }
            }
        });
        $request->session()->flash('alert-success', '¡Diccionario actualizado!');
        return redirect('diccionario');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
