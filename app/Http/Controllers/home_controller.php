<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App;
use App\Validacion;
use App\Diccionario;
use Auth;
use App\Http\Requests\home_request;

class home_controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Auth::check()) {
            return \View::make('home');
        }else{
            return \Redirect::to('/auth/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(home_request $request)
    {
        $archivo = $request->file('archivo');
        $excel = App::make('excel');
        $excel->setDelimiter(';');

        //Se declara global para que la función de excel pueda reconocer que la variable existe
        $GLOBALS['targets'] = [];
        $GLOBALS['id_targets'] = [];
        $names = [];
        $ids = [];
        $limit = $request->input('porcentual_limit');
        $GLOBALS['porcentual_limit'] = $limit;

        $excel->load($archivo, function($reader) {
            foreach ($reader->get() as $persona) {
                array_push($GLOBALS['targets'], $persona->nombre);
                array_push($GLOBALS['id_targets'], strval($persona->id));

                Validacion::create([
                    'identificacion' => $persona->id,
                    'nombre' => $persona->nombre
                ]);
            }
        });

        $resultados = [];
        $resultado_ids = [];

        $diccionario = Diccionario::All();
        foreach ($diccionario as $key => $value) {
            array_push($names, $value->nombre);
            array_push($ids, $value->identificacion);
        }
        //dd($ids);
        
        foreach ($GLOBALS['targets'] as $key => $target) {
            array_push($resultados, $this->similarities_func($names,$target));
        }

        foreach ($GLOBALS['id_targets'] as $key => $target) {
            array_push($resultado_ids, $this->similarities_func($ids,$target));
        }

        $request->session()->flash('alert-success', '¡Archivo de validación cargado!');
        return view('resultados', compact('resultados', 'resultado_ids', 'limit'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function similarities_func ($names, $target){
        //Se lleven a minúsculas los datos para evitar inconsistencias
        $names = array_map('strtolower', $names);
        $names = array_unique($names);
        $target = strtolower($target);

        //Se lleva el string a vector para obtener su tamaño con la función count
        $splitted_target = str_split($target);
        $targets = explode(" ", $target);

        //Inicialización de vector que contendrá las expresiones regulares
        $regular_expressions = [];
        $similarities = [];

        //Armando la matriz de expresiones regulares
        for ($k=0; $k < count($targets); $k++) {
            $value=$targets[$k];
            $temp2 = [];
            for ($i=0; $i < count(str_split($value)); $i++) { 
                $word = substr($value, $i);
                $len2 = count(str_split($word));
                $temp = [];
                for ($j=0; $j < $len2 ; $j++) {
                    $chunk = substr($word, 0, $j+1);
                    $chunk_reg_exp = '/('.$chunk.')+/';
                    array_push($temp, $chunk_reg_exp);
                }
                array_push($temp2, $temp);
            }
            $regular_expressions[$value] = $temp2;
        }
        //Cálculo de la similitud de las palabras
        foreach ($regular_expressions as $key => $regular_expressions_word) {
            $temp2 = [];
            foreach ($regular_expressions_word as $regular_expression){
                $temp = [];
                foreach ($regular_expression as $value) {
                    $matching_exp = preg_grep($value, $names);                
                    foreach ($matching_exp as $value) {
                        if(array_key_exists($value, $temp)){
                            $aux = $temp[$value];
                            $temp[$value] = $aux + 1;
                        }else{
                            $temp[$value] = 1;
                        }
                    }
                }
                foreach ($temp as $key2 => $value) {
                    
                    if(!(array_key_exists($key2, $temp2))){
                        $splitted_key = str_split($key2);
                        $temp2[$key2] = ($value);
                    }
                }
            }
            $similarities[$key] = $temp2;
        }
        $final = array();

        array_walk_recursive($similarities, function($item, $key) use (&$final){
            $final[$key] = isset($final[$key]) ?  $item + $final[$key] : $item;
        });

        $master_key = implode(' ', array_keys($similarities));
        $final_master = [];
        foreach ($final as $key => $value) {
            $splitted_key = str_split($key);
            $splitted_target_len_wspaces = count($splitted_target) - substr_count($target, ' ');
            $splitted_key_len_wspaces = count($splitted_key) - substr_count($key, ' ');
            $final[$key] = ($value/(max($splitted_target_len_wspaces,$splitted_key_len_wspaces)))*100;
        }
        arsort($final);
        $final = array_filter($final, function($value){
           if($value>=$GLOBALS['porcentual_limit']){
                return true;
            }else{
                return false;
            }
        });
        $final_master[$master_key] = $final;
        return ($final_master);   
    }
}
