<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Goutte\Client;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Diccionario;

class scrapping_controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $diccionario = Diccionario::All();
        return view('scrapping', compact('diccionario'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = new Client();
        $crawler = $client->request(
            'GET', 
            'http://web.worldbank.org/external/default/main?contentMDK=64069844&menuPK=116730&pagePK=64148989&piPK=64148984&querycontentMDK=64069700&theSitePK=84266'
        );
        $aux=0;
        $crawler->filter('table[class="tableBorderGrey"]')->each(function($element) {
          $element->filter('tr[class="altcolorcontentwhite"]')->each(function($nested_node) use (&$aux) {
            $diccionario = new Diccionario;
            $nested_node->filter('td')->each(function($nested_node2) use (&$aux, &$diccionario) {
                if ($aux == 0){
                    $diccionario->nombre = preg_replace( "/\r|\n/", "", $nested_node2->text());;
                }elseif ($aux == 1) {
                    $diccionario->address = preg_replace( "/\r|\n/", "", $nested_node2->text());; 
                }elseif ($aux == 2) {
                    $diccionario->country = preg_replace( "/\r|\n/", "", $nested_node2->text());;
                }elseif ($aux == 3) {                    
                    $diccionario->ineligibility_period_from = preg_replace( "/\r|\n/", "", $nested_node2->text());;
                }elseif ($aux == 4) {
                    $diccionario->ineligibility_period_to = preg_replace( "/\r|\n/", "", $nested_node2->text());;
                }elseif ($aux == 5) {
                    $diccionario->grounds = preg_replace( "/\r|\n/", "", $nested_node2->text());;
                }
                $aux = $aux + 1;
            });
            $registro = Diccionario::where('nombre', '=', $diccionario->nombre)
                                    ->where('address', '=', $diccionario->address)
                                    ->where('country', '=', $diccionario->country)
                                    ->where('ineligibility_period_from', '=', $diccionario->ineligibility_period_from)
                                    ->where('ineligibility_period_to', '=', $diccionario->ineligibility_period_to)
                                    ->where('grounds', '=', $diccionario->grounds)->first();
            if(!is_null($registro)){
                $registro->updated_at = $registro->freshTimestamp();
                $registro->save();
            }else{
                $diccionario->save();
            }            
            $aux = 0;
          });

          $element->filter('tr[class="altcolorcontentGrey"]')->each(function($nested_node) use (&$aux) {
            $diccionario = new Diccionario;
            $nested_node->filter('td')->each(function($nested_node2) use (&$aux, &$diccionario) {
                if ($aux == 0){
                    $diccionario->nombre = preg_replace( "/\r|\n/", "", $nested_node2->text());;
                }elseif ($aux == 1) {
                    $diccionario->address = preg_replace( "/\r|\n/", "", $nested_node2->text());; 
                }elseif ($aux == 2) {
                    $diccionario->country = preg_replace( "/\r|\n/", "", $nested_node2->text());;
                }elseif ($aux == 3) {                    
                    $diccionario->ineligibility_period_from = preg_replace( "/\r|\n/", "", $nested_node2->text());;
                }elseif ($aux == 4) {
                    $diccionario->ineligibility_period_to = preg_replace( "/\r|\n/", "", $nested_node2->text());;
                }elseif ($aux == 5) {
                    $diccionario->grounds = preg_replace( "/\r|\n/", "", $nested_node2->text());;
                }
                $aux = $aux + 1;
            });
            $registro = Diccionario::where('nombre', '=', $diccionario->nombre)
                                    ->where('address', '=', $diccionario->address)
                                    ->where('country', '=', $diccionario->country)
                                    ->where('ineligibility_period_from', '=', $diccionario->ineligibility_period_from)
                                    ->where('ineligibility_period_to', '=', $diccionario->ineligibility_period_to)
                                    ->where('grounds', '=', $diccionario->grounds)->first();
            if(!is_null($registro)){
                $registro->updated_at = $registro->freshTimestamp();
                $registro->save();
            }else{
                $diccionario->save();
            }            
            $aux = 0;
          });
        });
        $request->session()->flash('alert-success', '¡Diccionario actualizado!');
        return redirect('scrapping');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
