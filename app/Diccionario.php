<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diccionario extends Model
{
	protected $fillable = ['identificacion', 'nombre', 'address', 'country', 'ineligibility_period_from', 'ineligibility_period_to', 'grounds'];
    protected $table = 'diccionario';
}
