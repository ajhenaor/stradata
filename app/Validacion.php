<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Validacion extends Model
{
    protected $fillable = ['identificacion', 'nombre'];
    protected $table = 'validacion';
}
