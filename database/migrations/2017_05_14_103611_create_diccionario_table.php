<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiccionarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diccionario', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('identificacion',200)->nullable();
            $table->string('nombre',200);
            $table->string('address',200);
            $table->string('country',200);
            $table->string('ineligibility_period_from',200);
            $table->string('ineligibility_period_to',200);
            $table->string('grounds',200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('diccionario');
    }
}
