<!DOCTYPE html>
<html>
<head>
    <title>Text similarity calculator</title>
    <link rel="stylesheet" type="text/css" href="css/dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    @include('/js/dashboard')
</head>
<body>
@if (count($errors) > 0)
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Error</h4>
                </div>
                <div class="modal-body">
                    <p class="text-danger">
                        <small>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </small>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
@endif
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">
                    Text similarity calculator
                </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">            
                <ul class="nav navbar-nav navbar-right">
                    <li>{!! link_to("logout", $title = 'Logout') !!}</li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container-fluid main-container">
        <div class="col-md-2 sidebar">
            <ul class="nav nav-pills nav-stacked">
                <li id="home_li" class="active"><a id="home" href="home">Home</a></li>
                <li id="diccionario_li"><a id="diccionario" href="diccionario">Diccionario</a></li>
                <li id="scrapping_li"><a id="scrapping" href="scrapping">Scrapping</a></li>
            </ul>
        </div>
        @yield('content')    
    </div>
</body>
</html>