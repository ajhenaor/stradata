@extends('layouts.basic_alert')
@section('panel-content')
<div class="col-md-10 content">
    <div class="panel panel-default">
        <div class="panel-heading">
            <b>Diccionario</b>
        </div>
        <div class="panel-body">
         {!! Form::open(array('url'=>'diccionario', 'method'=>'post', 'files' => true)) !!}
            <div class="form-group row col-md-10">
                {!! Form::label('archivo_l', 'Diccionario') !!}
                {!! Form::file('archivo',null) !!}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-11">
                <p class="pull-right">
                    {!! Form::submit('Load' , array('class' => 'btn btn-primary')) !!}
                </p>
            </div>
        </div>
    </div>
    {!! Form::Close() !!}
</div>
@endsection