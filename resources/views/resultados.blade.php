@extends('layouts.basic_alert')
@section('panel-content')
<div class="col-md-10 content">
    <div class="panel panel-default">
        <div class="panel-heading">
            <b>Resultados</b>
        </div>
        <div class="panel-body">
	        <div class="row col-md-10">
		         {!! Form::open(array('url'=>'resultados', 'method'=>'post', 'files' => true)) !!}
		         {!! Form::label('info', 'El límite porcentual escogido fue: ') !!} {{$limit}}
	        </div>
			<table class="table table-striped table-responsive">
				<caption>Resultados de la búsqueda por nombres</caption>
			    <thead>
			      <tr>
			        <th>Nombre buscado</th>
			        <th>Nombre relacionado</th>
			        <th>Porcentaje de similitud</th>
			      </tr>
			    </thead>
			    <tbody>
			    	@foreach ($resultados as $value)
				    	@foreach ($value as $key1 => $word)
				    		@foreach($word as $key2 => $incidence)
							    <tr>
							    	<td>{!!$key1!!}</td>
							        <td>{!!$key2!!}</td>
							        <td>{!!$incidence!!}</td>
				      			</tr>
				    		@endforeach
				    	@endforeach
			    	@endforeach
			    </tbody>
			 </table>

			<table class="table table-striped table-responsive">
				<caption>Resultados de la búsqueda por identificación</caption>
			    <thead>
			      <tr>
			        <th>Id buscado</th>
			        <th>Id relacionado</th>
			        <th>Porcentaje de similitud</th>
			      </tr>
			    </thead>
			    <tbody>
			    	@foreach ($resultado_ids as $value)
				    	@foreach ($value as $key1 => $word)
				    		@foreach($word as $key2 => $incidence)
							    <tr>
							    	<td>{!!$key1!!}</td>
							        <td>{!!$key2!!}</td>
							        <td>{!!$incidence!!}</td>
				      			</tr>
				    		@endforeach
				    	@endforeach
			    	@endforeach
			    </tbody>
			 </table>

        </div>
    </div>
    {!! Form::Close() !!}
</div>
@endsection