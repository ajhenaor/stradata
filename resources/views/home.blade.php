@extends('layouts.basic_alert')
@section('panel-content')
<div class="col-md-10 content">
    <div class="panel panel-default">
        <div class="panel-heading">
            <b>Home</b>
        </div>
        <div class="panel-body">
         {!! Form::open(array('url'=>'home', 'method'=>'post', 'files' => true)) !!}
            <div class="form-group row col-md-10">
                {!! Form::label('similitud_l', 'Porcentaje de similitud mínimo') !!}
                {!! Form::selectRange('porcentual_limit', 1, 100, ['value'=>85])!!}
            </div>
            <div class="form-group row col-md-10">
                {!! Form::label('archivo_l', 'Archivo de validación') !!}
                {!! Form::file('archivo',null) !!}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-11">
                <p class="pull-right">
                    {!! Form::submit('Load' , array('class' => 'btn btn-primary')) !!}
                </p>
            </div>
        </div>
    </div>
    {!! Form::Close() !!}
</div>
@endsection