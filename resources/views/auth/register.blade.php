<link rel="stylesheet" type="text/css" href="/css/dashboard.css">
<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
@include('js/register')
<!-- resources/views/auth/register.blade.php -->
@if (count($errors) > 0)
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Error</h4>
                </div>
                <div class="modal-body">
                    <p class="text-danger">
                        <small>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </small>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
@endif

<div class="container" style="margin-top:30px">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><strong>Register </strong></h3></div>
                <div class="panel-body">
                    <form method="POST" action="/auth/register">
                        <div>
                            Name
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                        </div>

                        <div>
                            Email
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                        </div>

                        <div>
                            Password
                            <input type="password" class="form-control" name="password">
                        </div>

                        <div>
                            Confirm Password
                            <input type="password" class="form-control" name="password_confirmation">
                        </div>
                        <br>
                        <div class="form-group">
                            {!! link_to("auth/login", $title = 'Login') !!}
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-sm btn-default">Register</button>
                        </div>
                    </form>
                </div>

        </div>
    </div>
</div>