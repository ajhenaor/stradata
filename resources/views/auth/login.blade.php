<!DOCTYPE html>
<html>
<head>
    @include('/js/login')
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="/css/dashboard.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
</head>
<body>
@if (count($errors) > 0)
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Error</h4>
                </div>
                <div class="modal-body">
                    <p class="text-danger">
                        <small>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </small>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="container" style="margin-top:30px">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><strong>Sign in </strong></h3></div>
                <div class="panel-body">
                    <form method="POST" action="/auth/login">
                        <div class="form-group">
                            Email
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                        </div>

                        <div class="form-group">
                            Password
                            <input type="password" class="form-control" name="password" id="password">
                        </div>

                        <div class="form-group">
                            <input type="checkbox" name="remember"> Remember Me
                        </div>

                        <div class="form-group">
                            {!! link_to("auth/register", $title = 'Register') !!}
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-sm btn-default">Login</button>
                        </div>
                    </form>

                </div>

        </div>
    </div>
</div>

</body>
</html>