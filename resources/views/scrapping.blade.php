@extends('layouts.basic_alert')
@section('panel-content')
<div class="col-md-10 content">
    <div class="panel panel-default">
        <div class="panel-heading">
            <b>Scrapping</b>
        </div>
        <div class="body">
        <br>
            {!! Form::open(array('url'=>'scrapping', 'method'=>'post')) !!}
            <div class="col-md-12">
                <p class="pull-right">
                    {!! Form::submit('Update' , array('class' => 'btn')) !!}
                </p>
            </div>
            {!! Form::Close() !!}
            <table class="table table-striped table-responsive">
                <thead>
                  <tr>
                    <th>Firm name</th>
                    <th>Addres</th>
                    <th>Country</th>
                    <th>Ineligibility period - from</th>
                    <th>Ineligibility period - to</th>
                    <th>Grounds</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($diccionario as $entry)
                        <tr>
                            <td>{{$entry->nombre}}</td>
                            <td>{{$entry->address}}</td>
                            <td>{{$entry->country}}</td>
                            <td>{{$entry->ineligibility_period_from}}</td>
                            <td>{{$entry->ineligibility_period_to}}</td>
                            <td>{{$entry->grounds}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            
        </div>
    </div>
</div>
@endsection